module.exports = {
  siteMetadata: {
    title: `Engleezi - It's so Easy`,
    description: `Affordable, accessible and fun, Engleezi is an English tutoring service that aims to make your child a better and more fluent English speaker. Our unique online approach gets your children learning English one-on-one from a native English teacher from the comfort of your home.`,
    author: `Suleiman Mayow <mayow.sullom@gmail.com>`,
    siteUrl: `https://en.myengleezi.com`
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    `gatsby-plugin-sitemap`,
    `gatsby-plugin-catch-links`,
    {
      resolve: 'gatsby-plugin-i18n',
      options: {
        langKeyDefault: 'en',
        useLangKeyLayout: false,
      },
    },
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: 'UA-130216069-21',
        // Puts tracking script in the head instead of the body
        head: true,
        anonymize: true,
        respectDNT: true,
        exclude: ['/404/', 'terms-of-service', 'policy', 'blog'],
        sampleRate: 5,
        siteSpeedSampleRate: 10,
        cookieDomain: 'myengleezi.com',
      },
    },



    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Engleezi-website`,
        short_name: `Engleezi`,
        start_url: `/`,
        background_color: `#0b3880`,
        theme_color: `#0b3880`,
        display: `standalone`,
        icon: `src/images/engleezi-icon.png`, // This path is relative to the root of the site.
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.app/offline
    // 'gatsby-plugin-offline',
  ],
}
