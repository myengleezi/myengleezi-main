import React from 'react'
import ReactCountryFlag from "react-country-flag";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from 'reactstrap'

class TopNav extends React.Component {
  constructor(props) {
    super(props)

    this.toggle = this.toggle.bind(this)
    this.state = {
      isOpen: false,
    }
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen,
    })
  }
  render() {
    let linksMarkup = this.props.links.map((link, index) => {
      let linkMarkup = link.active ? (
        <NavLink className="grey-co" href={link.link} className="active">
          {link.label}
        </NavLink>
      ) : (
          <NavLink className="" href={link.link}>
            {link.label}
          </NavLink>
        )
      return <NavItem key={index}>{linkMarkup}</NavItem>
    })

    return (
      <div>
        <Navbar color="light" light expand="lg">
          <div className="container">
            <NavbarBrand className="brand float-left" href="/">
              <img
                src={this.props.logo}
                className="headerLogo"
                alt="Engleezi Logo"
              />
            </NavbarBrand>
            <NavbarToggler className="blue-bg" onClick={this.toggle} />
            <Collapse isOpen={this.state.isOpen} navbar>
              <Nav
                className="ml-auto navbarMenu wt-bg pl-3 pr-3 pt-3 pb-3"
                navbar
              >
                {linksMarkup}
              </Nav>
              <Nav
                className="ml-auto navbarMenu blue-bg pl-3 pr-3 pt-3 pb-3"
                navbar
              >
                <NavItem>
                  <NavLink
                    className="white-co"
                    href="https://student.myengleezi.com/HumanResource/login.aspx"
                    target="_blank"
                  >
                    Login
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    className="white-co"
                    href="https://student.myengleezi.com/HumanResource/login.aspx"
                    target="_blank"
                  >
                    Register
                  </NavLink>
                </NavItem>

                <UncontrolledDropdown nav inNavbar>
                  <DropdownToggle className="white-co" nav caret>
                    Languages
                  </DropdownToggle>
                  <DropdownMenu right>
                  <DropdownItem>
                      <NavLink  className="grey-co" href="/">
                      <ReactCountryFlag 
                    styleProps={{
                        width: '30px',
                        height: '30px',
                        marginRight:'10%'
                    }}
                    code="us"
                    svg
                />
                 English
                    
                     
                      </NavLink>
                      </DropdownItem>
                      
                      <DropdownItem divider />

                    <DropdownItem>
                      <NavLink
                        className="grey-co"
                        href="https://ar.myengleezi.com/"
                      >
                      <ReactCountryFlag 
                    styleProps={{
                        width: '30px',
                        height: '30px',
                        marginRight:'10%'
                    }}
                    code="sa"
                    svg
                />
  العربية                      </NavLink>
                    </DropdownItem>
                    <DropdownItem divider />
                    <DropdownItem>
                      <NavLink  className="grey-co" href="https://tr.myengleezi.com">
                      <ReactCountryFlag 
                    styleProps={{
                        width: '30px',
                        height: '30px',
                        marginRight:'10%'
                    }}
                    code="tr"
                    svg
                />
                 
Türkçe

                    
                     
                      </NavLink>
                      </DropdownItem>
                  </DropdownMenu>
                </UncontrolledDropdown>
              </Nav>
            </Collapse>
          </div>
        </Navbar>
      </div>
    )
  }
}
export default TopNav
