import React from 'react'
import './header.css'
import logo from './logo.png'
import TopNav from './topNav'

function Header(props) {
  return (
    <header style={headStyle}>
      <TopNav links={props.links} logo={logo} />
    </header>
  )
}
export default Header

const headStyle = {  width: '100%', zIndex: '5' }
