import { Link } from 'gatsby'
import React from 'react'
import './footer.css'
import footerBg from './footerBg.svg'
import subscribeBackground from './subscribeBg.png'
import engleeziLogo from './engleeziLogo.png'
import { Icon } from 'react-icons-kit'
import { ic_mail_outline, ic_stay_current_portrait } from 'react-icons-kit/md/'
import { facebook, youtube, twitter, instagram } from 'react-icons-kit/fa'
import { arrowRight2 } from 'react-icons-kit/icomoon/arrowRight2'

const Footer = () => (
  <footer>
    <div
      className="banner-creative bg-img cover-background no-padding-bottom"
      style={divStyle}
    >
      <div className="container footer-pad">
        <div className="row">
          <div className="box" style={{ display: 'flex' }}>
            <div className="col-lg-5 col-md-4 auto-margin" id="textBox">
              <p
                className="text-left margin-three-left"
                style={{ textAlign: 'justify', color: '#525051' }}
              >
                Try our complimentary trial lesson to experience our unique
                curriculums! Yes! Just sign up and try out a full class for
                free!
              </p>
            </div>
            <div
              className="col-lg-7 col-md-8 col-sm-12 small-view"
              style={subscribeBg}
            >
              <div
                className="email-box  wow fadeInUp"
                style={{
                  visibility: 'visible',
                  animationDelay: '0.3s',
                  animationName: 'fadeInUp',
                  top: '35%',
                  right: '2%',
                }}
              >
                <div className="input float-right col-md-8">
                  <a
                    href="https://student.myengleezi.com/HumanResource/login.aspx"
                    rel="noopener noreferrer"
                    target="_blank"
                    className="position-relative col-md-12 butn"
                  >
                    <span className="text-center">Join</span>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="divider" />
        <div className="row">
          <div className="col-md-4 padding-six-bottom">
            <img
              src={engleeziLogo}
              alt="Engleezi Logo file"
              className="footerImage"
            />
            <p className="small-disapear text-justify fontSizeP">
              Engleezi is an innovative, state of the art online learning
              platform that facilitates high quality, one-on-one English lessons
              for children. Not only are our classes fun and intuitive, they are
              also designed to be both affordable and convenient for everyone.
              With Engleezi, your child can now learn English from the comfort
              of your own home.
            </p>
          </div>
          <div className="col-md-2 padding-six-bottom small-disapear">
            <h5 className=" padding-four-bottom white-co fontSizeh5">
              Quick Nav
            </h5>
            <Link className="quickNav" to="/about-us/">
              <p className="fontSizeP">About Us</p>
            </Link>

            <a
              className="quickNav padding-two-bottom"
              href="https://blog.myengleezi.com/en/"
              target="_blank"
              rel="noopener noreferrer"
            >
              <p className="fontSizeP">Engleezi Blog</p>
            </a>

            <a
              className="quickNav padding-two-bottom"
              href="https://www.youtube.com/channel/UCFUvVaTVIMb0IqKK2lIMC0g/"
              target="_blank"
              rel="noopener noreferrer"
            >
              <p className="fontSizeP">Resources</p>
            </a>
            <Link
              className="quickNav padding-two-bottom"
              to="/terms-of-service/"
            >
              <p className="fontSizeP">Terms of Service</p>
            </Link>

            <Link className="quickNav padding-two-bottom" to="/policy/">
              <p className="fontSizeP">Privacy Policy</p>
            </Link>
          </div>
          <div className="col-md-1" />
          <div className="col-md-3 padding-six-bottom">
            <h5 className="padding-two-bottom white-co fontSizeh5">
              Contact Us{' '}
            </h5>
            <div className="row">
              <span className="col-md-2 col-sm-3">
                <Icon size={32} icon={ic_mail_outline} />
              </span>
              <p className="col-md-10 col-sm-9">
                {' '}
                <a href="mailto:info@myengleezi.com" className="quickNav">
                  info@myengleezi.com
                </a>
              </p>
              <span className="col-md-2 col-sm-3">
                <Icon size={32} icon={ic_stay_current_portrait} />
              </span>
              <p className="col-md-10 col-sm-9 fontSizeP">
                {' '}
                <a href="tel:0060143082591" className="quickNav">
                 +60143082591
                </a>{' '}
              </p>
            </div>
          </div>
          <div className="col-md-2">
            <h5 className="padding-four-bottom white-co fontSizeh5">
              Follow Us
            </h5>
            <span className="margin-five-right ">
              <a
                href="https://www.facebook.com/myengleezi"
                target="_blank"
                rel="noopener noreferrer"
              >
                <Icon
                  size={23}
                  icon={facebook}
                  style={{ color: 'white' }}
                  className="socialIcons social margin-three-bottom"
                />
              </a>
            </span>
            <span className="margin-five-right ">
              <a
                href="https://www.twitter.com/myengleezi"
                target="_blank"
                rel="noopener noreferrer"
              >
                <Icon
                  size={23}
                  icon={twitter}
                  style={{ color: 'white' }}
                  className="socialIcons social margin-three-bottom"
                />
              </a>
            </span>
            <span className="margin-five-right">
              <a
                href="https://www.youtube.com/channel/UCFUvVaTVIMb0IqKK2lIMC0g/"
                target="_blank"
                rel="noopener noreferrer"
              >
                <Icon
                  size={23}
                  style={{ color: 'white' }}
                  icon={youtube}
                  className="socialIcons social margin-three-bottom"
                />
              </a>
            </span>
            <span className="margin-five-right">
              <a
                href="https://www.instagram.com/myengleezi"
                target="_blank"
                rel="noopener noreferrer"
              >
                <Icon
                  size={23}
                  style={{ color: 'white' }}
                  icon={instagram}
                  className="socialIcons social margin-three-bottom"
                />
              </a>
            </span>
          </div>
        </div>
      </div>
    </div>
    <div className="copyRight text-center " style={rightsReserved}>
      {' '}
      <p className="poppins">
        � {new Date().getFullYear()} Engleezi All rights reserved{' '}
      </p>
    </div>
    <div className="promotion-box" style={promotionBar}>
      <a href="https://student.myengleezi.com/HumanResource/login.aspx" target="_blank"
        rel="noopener noreferrer">
        <p className="row poppins co-white ">
          <div className="container">
            <div className="row">
              <p className="col-md-6 col-sm-7 co-white poppins text-left pr-4">
               Try a FREE trial class today! After that, it’s just $5/class - no hidden fees, no subscriptions, no contracts!     </p>
              <span className="col-md-4 col-sm-5 text-center">
                <button className="button-light" style={promoButton}>
                  <span
                    className=" poppins text-button text-black col-md-8"
                    style={{ color: 'black' }}
                  >  Register
            </span>
                  <span className="co-blue">
                    <Icon
                      size={25}
                      icon={arrowRight2}
                      className="fas fa-arrow-right text-left display-inline col-md-4"
                    />
                  </span>
                </button>
              </span>
            </div>
          </div>

        </p>
        <a
          href="https://student.myengleezi.com/HumanResource/login.aspx"
          target="_blank"
          rel="noopener noreferrer"
        >

        </a>
      </a>
    </div>
  </footer>
)

const divStyle = {
  backgroundImage: 'url(' + footerBg + ')',
}
const subscribeBg = {
  backgroundImage: 'url(' + subscribeBackground + ')',
  padding: 0,
  backgroundRepeat: 'no-repeat',
  backgroundSize: '120%',
  borderRadius: '12px',
}
const rightsReserved = {
  position: 'relative',
  zIndex: '0',
}
const promotionBar = {
  position: 'fixed',
  bottom: '0'
}
const promoButton = {
  height: '48px',
  width: '184px'
}

export default Footer
