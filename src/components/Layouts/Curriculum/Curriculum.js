import React, { Component } from 'react'
import Structure from './Structure/structure'
import Result from './Results/result'
import Bannar from './Bannar/banner'
import Particles from 'react-particles-js'
import ParticlesBubble from '../ParticlesBubble'

class Curriculum extends Component {
  render() {
    const CurriculumDataAr = this.props.data
    return (
      <>
        <Particles
          style={{ position: 'fixed', zIndex: 0 }}
          params={ParticlesBubble}
        />
        <Bannar
          header={CurriculumDataAr.Bannar.headerTextH1}
          para={CurriculumDataAr.Bannar.paragraphTextP}
          button={CurriculumDataAr.Bannar.buttonText}
        />
        <Result
          header={CurriculumDataAr.Result.header}
          text={CurriculumDataAr.Result.text}
        />
        <Structure
          header={CurriculumDataAr.Structure.header}
          text={CurriculumDataAr.Structure.text}
        />
      </>
    )
  }
}
export default Curriculum
