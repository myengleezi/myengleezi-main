import React from 'react'
import bannerRight from './Banner.png'
import { Icon } from 'react-icons-kit'
import { arrowRight2 } from 'react-icons-kit/icomoon/arrowRight2'

const Banner = props => (
  <section
    style={{
      width: '100%',
      paddingTop: '0',
    }}
  >
    <div className="main-banner-area">
      <div className="right-bg">
        <img
          src={bannerRight}
          className="img-fluid float-right width-100"
          alt=""
        />
      </div>

      <div className="header-text">
        <div className="container">
          <div className="row">
            <div className="col-lg-5 col-md-12 col-sm-12">
              <h1
                className="co-blue text-left  circularstd-bold wow fadeInUp"
                data-wow-delay=".1s"
              >
                {props.header}
              </h1>
              <p
                className="poppins text-left wow fadeInUp"
                data-wow-delay=".2s"
              >
                {props.para}
              </p>

              <div className="divider-full" />
              <div className="fadeInUp" data-wow-delay=".3s">
                <div className="input">
                  <a
                    href="https://student.myengleezi.com/HumanResource/login.aspx"
                    target="_blank"
                  >
                    <button className="button-light">
                      <span
                        className=" CircularStd-Medium text-button text-black col-md-8"
                        style={{ color: 'black' }}
                      >
                        {props.button}
                      </span>
                      <span className="co-blue">
                        <Icon
                          size={25}
                          icon={arrowRight2}
                          className="fas fa-arrow-right text-left display-inline col-md-4"
                        />
                      </span>
                    </button>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
)

export default Banner
