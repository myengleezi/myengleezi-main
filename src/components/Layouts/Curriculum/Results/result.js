import React from 'react'
import resultsImg from './result.png'

const Result = props => (
  <section>
    <div className="container">
      <div className="row">
        <div className=" col-md-4 col-sm-12">
          <img
            src={resultsImg}
            alt="Get Instant results with Engleezi"
            className="col-sm-8 col-md-12 "
          />
        </div>
        <div className="offset-md-1 col-md-7 col-sm-12 margin-ten-top">
          <h2 className="margin-five-bottom text-left engleezi-black">
            {props.header}
          </h2>

          <p className="font-weight-400 text-left  engleezi-black">
            {props.text}
          </p>
        </div>
      </div>
    </div>
  </section>
)

export default Result
