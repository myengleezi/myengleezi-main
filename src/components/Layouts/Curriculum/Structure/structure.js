import React, { Component } from 'react'
import structureImg from './structure.png'
export class Structure extends Component {
  render() {
    return (
      <section>
        <div className="container">
          <div className="d-flex flex-md-row-reverse row">
            <div className=" col-md-4 col-sm-12">
              <img
                src={structureImg}
                alt="Get Instant results with Engleezi"
                className="col-sm-8 col-md-12 "
              />
            </div>
            <div className="offset-md-1 col-md-7 col-sm-12 margin-ten-top">
              <h2 className="margin-five-bottom engleezi-black text-left">
                {this.props.header}
              </h2>

              <p className="font-weight-400 engleezi-black text-left">
                {this.props.text}
              </p>
            </div>
          </div>
        </div>
      </section>
    )
  }
}

export default Structure
