import React, { Component } from 'react'
import Particles from 'react-particles-js'
import ParticlesBubble from '../ParticlesBubble'
import Bannar from './Bannar/banner'

class About extends Component {
  render() {
    const AboutDataAr = this.props.data
    return (
      <div>
        <Particles
          style={{ position: 'fixed', zIndex: 0 }}
          params={ParticlesBubble}
        />
        <Bannar
          header={AboutDataAr.Bannar.headerTextH1}
          para={AboutDataAr.Bannar.paragraphTextP}
        />
      </div>
    )
  }
}

export default About
