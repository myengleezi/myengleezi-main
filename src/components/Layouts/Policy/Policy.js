import React, { Component } from 'react'

class Policy extends Component {
  render() {
    return (
      <div>
        <div className="">
          {' '}
          <div className="container padding-ten-top">
            <div className="row">
              <h1>
                <span style={{ fontWeight: 400 }}>Privacy Statements</span>
              </h1>
              <div style={{ width: '100%' }} />
              <p>&nbsp;</p>
              <h4>
                <span style={{ fontWeight: 400 }}>
                  Learn about our commitment to privacy protection
                </span>
              </h4>
              <div style={{ width: '100%' }} />
              <p>&nbsp;</p>
              <h3 style={{ textAlign: 'center' }}>
                <span style={{ fontWeight: 400 }}>
                  Privacy Statement, effective as of March 1, 2019
                </span>
              </h3>
              <p>
                <span style={{ fontWeight: 400 }}>
                  Engleezi Sdn Bhd. (“Engleezi” or the “Company”) is committed
                  to protecting the privacy of individuals who visit the
                  Company’s Web sites (“Visitors”), individuals who register to
                  use the Services as defined below (“Customers”), and
                  individuals who register to attend the Company’s corporate
                  events (“Attendees”). This Privacy Statement describes
                  Engleezi’s privacy practices in relation to the use of the
                  Company’s Web sites and the related applications and services
                  offered by Engleezi (collectively, the “Services”).
                </span>
              </p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <h4>1. Web sites covered</h4>
              <p>&nbsp;</p>
              <p>
                <span style={{ fontWeight: 400 }}>
                  This Privacy Statement covers the information practices of Web
                  sites that link to this Privacy Statement, including:{' '}
                </span>
                <a href="http://www.myengleezi.com/">
                  <span style={{ fontWeight: 400 }}>
                    http://www.myengleezi.com
                  </span>
                </a>
                <span style={{ fontWeight: 400 }}>
                  ; (referred to as “Engleezi.com’s Web sites” or “the Company’s
                  Web sites”).
                </span>
              </p>
              <p>
                <span style={{ fontWeight: 400 }}>
                  Engleezi’s Web sites may contain links to other Web sites. The
                  information practices or the content of such other Web sites
                  is governed by the privacy statements of such other Web sites.
                  The Company encourages you to review the privacy statements of
                  other Web sites to understand their information practices.
                </span>
              </p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <h4>2. Information collected</h4>
              <p>&nbsp;</p>
              <p>
                <span style={{ fontWeight: 400 }}>
                  When expressing an interest in obtaining additional
                  information about the Services, or registering to use the Web
                  sites or other Services, or registering for an event, Engleezi
                  requires you to provide the Company with personal contact
                  information, such as full name, email address , company name,
                  and phone number (as “Required Contact Information”). When
                  purchasing the Services or registering for an event, Engleezi
                  may also require you to provide the Company with financial
                  qualification and billing information, such as billing name
                  and address, credit card number, and the number of employees
                  within the organization that will be using the Services
                  (“Billing Information”). Engleezi may also ask you to provide
                  additional information, such as company annual revenues,
                  number of employees, or industry (“Optional Information”).
                  When Visitors apply for a job with the Company, Engleezi may
                  also require you to submit additional personal information as
                  well as a resume or curriculum vitae (“Applicant
                  Information”). Required Contact Information, Billing
                  Information, Applicant Information, Optional Information and
                  any other information you submit to Engleezi to or through the
                  Services are referred to collectively as “Data.”
                </span>
              </p>
              <p>
                <span style={{ fontWeight: 400 }}>
                  As you navigate the Company’s Web sites, Engleezi may also
                  collect information through the use of commonly—used
                  information—gathering tools, such as cookies and Web Analytics
                  (“Web Site Navigational Information”). Web Site Navigational
                  Information includes standard information from your Web
                  browser (such as browser type and browser language), your
                  Internet Protocol (“IP”) address, and the actions you take on
                  the Company’s Web sites (such as the Web pages viewed and the
                  links clicked).{' '}
                </span>
              </p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <h4>3. Use of information collected</h4>
              <p>&nbsp;</p>
              <p>
                <span style={{ fontWeight: 400 }}>
                  The Company uses Data about Engleezi Customers to perform the
                  services requested. For example, if you fill out a “Contact
                  Me” Web form, the Company will use the information provided to
                  contact you about your interest in the Services.
                </span>
              </p>
              <p>
                <span style={{ fontWeight: 400 }}>
                  The Company also uses Data about Engleezi Attendees to plan
                  and host corporate events, host online forums and social
                  networks in which event attendees may participate, and to
                  populate online profiles for Attendees on the Company’s Web
                  sites. Additional information on the Company’s privacy
                  practices with respect to Data about Engleezi Attendees may be
                  found in additional privacy statements on the event Web sites,
                  as the case may be.{' '}
                </span>
              </p>
              <p>
                <span style={{ fontWeight: 400 }}>
                  The Company may also use Data about Engleezi Customers and
                  Data about Engleezi Attendees for marketing purposes. For
                  example, the Company may use information you provide to
                  contact you to further discuss your interest in the Services
                  and to send you information regarding the Company, its
                  affiliates, and its partners, such as information about
                  promotions or events.
                </span>
              </p>
              <p>
                <span style={{ fontWeight: 400 }}>
                  Engleezi uses credit card information solely to check the
                  financial qualifications and collect payment from prospective
                  Customers and Attendees.
                </span>
              </p>
              <p>
                <span style={{ fontWeight: 400 }}>
                  Engleezi uses Web Site Navigational Information to operate and
                  improve the Company’s Web sites. The Company may also use Web
                  Site Navigational Information alone or in combination with
                  Data about Engleezi Customers and Data about Engleezi
                  Attendees to provide personalized information about the
                  Company.{' '}
                </span>
              </p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <h4>
                4. Public forums, refer a friend, and customer testimonials
              </h4>
              <p>&nbsp;</p>
              <p>
                <span style={{ fontWeight: 400 }}>
                  Engleezi may provide bulletin boards, blogs, or chat rooms on
                  the Company’s Web sites. Any personal information you choose
                  to submit in such a forum may be read, collected, or used by
                  others who visit these forums, and may be used to send you
                  unsolicited messages. Engleezi is not responsible for the
                  personal information you choose to submit in these forums.
                </span>
              </p>
              <p>
                <span style={{ fontWeight: 400 }}>
                  Customers and Visitors may elect to use the Company’s referral
                  program to inform friends about the Company’s Web sites. When
                  using the referral program, the Company requests the friend’s
                  name and email address. Engleezi will automatically send the
                  friend a one–time email inviting him or her to visit the
                  Company’s Web sites. Engleezi does not store this information.
                </span>
              </p>
              <p>
                <span style={{ fontWeight: 400 }}>
                  Engleezi posts a list of Customers and testimonials on the
                  Company’s Web sites that contain information such as Customer
                  names and titles. Engleezi obtains the consent of each
                  Customer prior to posting any information on such a list or
                  posting testimonials.
                </span>
              </p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <h4>5. Web Site Navigational Information</h4>
              <p>&nbsp;</p>
              <p>
                <span style={{ fontWeight: 400 }}>
                  Cookies, Web Analytics and IP Addresses
                </span>
              </p>
              <p>
                <span style={{ fontWeight: 400 }}>
                  Engleezi uses commonly—used information—gathering tools, such
                  as cookies and Web Analytics, to collect information as you
                  navigate the Company’s Web sites (“Web Site Navigational
                  Information”). This section describes the types of Web Site
                  Navigational Information used on the Company’s Web sites and
                  how this information may be used.
                </span>
              </p>
              <p>
                <b>Cookies</b>
              </p>
              <p>
                <span style={{ fontWeight: 400 }}>
                  Engleezi.com uses cookies to make interactions with the
                  Company’s Web sites easy and meaningful. When you visit one of
                  the Company’s Web sites, Engleezi.com’s servers send a cookie
                  to your computer. Standing alone, cookies do not personally
                  identify you; they merely recognize your Web browser. Unless
                  you choose to identify yourself to Engleezi.com, either by
                  responding to a promotional offer, opening an account, or
                  filling out a Web form (such as a “Contact Me” or a “30 Day
                  Free Trial” Web form), you remain anonymous to the Company.
                </span>
              </p>
              <p>
                <span style={{ fontWeight: 400 }}>
                  Engleezi.com uses cookies that are session—based and
                  persistent—based. Session cookies exist only during one
                  session. They disappear from your computer when you close your
                  browser software or turn off your computer. Persistent cookies
                  remain on your computer after you close your browser or turn
                  off your computer. Please note that if you disable your Web
                  browser’s ability to accept cookies, you will be able to
                  navigate the Company’s Web sites, but you will not be able to
                  successfully use the Services.
                </span>
              </p>
              <p>
                <span style={{ fontWeight: 400 }}>
                  The following sets out how Engleezi.com uses different
                  categories of cookies and your options for managing cookies’
                  settings:
                </span>
              </p>
              <p>&nbsp;</p>
              <table>
                <tbody>
                  <tr>
                    <td>
                      <b>Type of Cookies</b>
                    </td>
                    <td>
                      <b>Description</b>
                    </td>
                    <td>
                      <b>Managing Settings</b>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <span style={{ fontWeight: 400 }}>Required cookies</span>
                    </td>
                    <td>
                      <span style={{ fontWeight: 400 }}>
                        Required cookies enable you to navigate the Company’s
                        Web sites and use its features, such as accessing secure
                        areas of the Web sites and using Engleezi Services.
                      </span>
                      <p />
                      <p>
                        <span style={{ fontWeight: 400 }}>
                          If you have chosen to identify yourself to Engleezi,
                          the Company uses cookies containing encrypted
                          information to allow the Company to uniquely identify
                          you. Each time you log into the Services, a cookie
                          containing an encrypted, unique identifier that is
                          tied to your account is placed on your browser. These
                          cookies allow the Company to uniquely identify you
                          when you are logged into the Services and to process
                          your online transactions and requests.
                        </span>
                      </p>
                    </td>
                    <td>
                      <span style={{ fontWeight: 400 }}>
                        Because required cookies are essential to operate the
                        Company’s Web sites and the Services, there is no option
                        to opt out of these cookies.
                      </span>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <span style={{ fontWeight: 400 }}>
                        Performance cookies
                      </span>
                    </td>
                    <td>
                      <span style={{ fontWeight: 400 }}>
                        These cookies collect information about how Visitors use
                        our Website, including which pages visitors go to most
                        often and if they receive error messages from certain
                        pages. These cookies do not collect information that
                        individually identifies a Visitor. All information these
                        cookies collect is aggregated and anonymous. It is only
                        used to improve how the Company’s Web site functions and
                        performs.
                      </span>
                      <p />
                      <p>
                        <span style={{ fontWeight: 400 }}>
                          From time—to—time, Engleezi engages third parties to
                          track and analyze usage and volume statistical
                          information from individuals who visit the Company’s
                          Web sites. Engleezi may also utilize Flash cookies for
                          these purposes.
                        </span>
                      </p>
                    </td>
                    <td />
                  </tr>
                  <tr>
                    <td>
                      <span style={{ fontWeight: 400 }}>
                        Functionality cookies
                      </span>
                    </td>
                    <td>
                      <span style={{ fontWeight: 400 }}>
                        Functionality cookies allow the Company’s Web sites to
                        remember information you have entered or choices you
                        make (such as your username, language, or your region)
                        and provide enhanced, more personal features.
                        &nbsp;These cookies also enable you to optimize your use
                        of Engleezi’s Services after logging in. These cookies
                        can also be used to remember changes you have made to
                        text size, fonts and other parts of web pages that you
                        can customize.
                      </span>
                      <p />
                      <p>
                        <span style={{ fontWeight: 400 }}>
                          Engleezi uses local shared objects, also known as
                          Flash cookies, to store your preferences or display
                          content based upon what you view on our Websites to
                          personalize your visit.
                        </span>
                      </p>
                    </td>
                    <td />
                  </tr>
                  <tr>
                    <td>
                      <span style={{ fontWeight: 400 }}>
                        Targeting or Advertising cookies
                      </span>
                    </td>
                    <td>
                      <span style={{ fontWeight: 400 }}>
                        From time—to—time, Engleezi engages third parties to
                        track and analyze usage and volume statistical
                        information from individuals who visit the Company’s Web
                        sites. &nbsp;Engleezi sometimes uses cookies delivered
                        by third parties to track the performance of Company
                        advertisements. &nbsp;For example, these cookies
                        remember which browsers have visited the Company’s Web
                        sites. The information provided to third parties does
                        not include personal information, but this information
                        may be re—associated with personal information after the
                        Company receives it.
                      </span>
                      <p />
                      <p>
                        <span style={{ fontWeight: 400 }}>
                          Engleezi also contracts with third—party advertising
                          networks that collect IP addresses and other
                          information from Web Analytics (see below) on the
                          Company’s Web sites, from emails, and on third—party
                          Web sites. Ad networks follow your online activities
                          over time and across different sites or other online
                          services by collecting Web Site Navigational
                          Information through automated means, including through
                          the use of cookies. These technologies may recognize
                          you across the different devices you use, such as a
                          desktop or laptop computer, smartphone or tablet.
                          Third parties use this information to provide
                          advertisements about products and services tailored to
                          your interests. You may see these advertisements on
                          other Websites or mobile applications on any of your
                          devices. This process also helps us manage and track
                          the effectiveness of our marketing efforts. Third
                          parties, with whom the Company partners to provide
                          certain features on our Web sites or to display
                          advertising based upon your Web browsing activity, use
                          Flash cookies to collect and store information.
                          &nbsp;Flash cookies are different from browser cookies
                          because of the amount of, type of, and how data is
                          stored.
                        </span>
                      </p>
                    </td>
                    <td />
                  </tr>
                </tbody>
              </table>
              <p>&nbsp;</p>
              <p>
                <b>Web Analytics</b>
              </p>
              <p>
                <span style={{ fontWeight: 400 }}>
                  Engleezi uses Web Analytics alone or in conjunction with
                  cookies to compile information about Customers and Visitors’
                  usage of the Company’s Web sites and interaction with emails
                  from the Company. Web Analytics are clear electronic images
                  that can recognize certain types of information on your
                  computer, such as cookies, when you viewed a particular
                  Website tied to the Web beacon, and a description of a Web
                  site tied to the Web beacon. For example, Engleezi may place
                  Web Analytics in marketing emails that notify the Company when
                  you click on a link in the email that directs you to one of
                  the Company’s Web sites. Engleezi uses Web Analytics to
                  operate and improve the Company’s Web sites and email
                  communications.
                </span>
              </p>
              <p>&nbsp;</p>
              <p>
                <b>IP Addresses</b>
              </p>
              <p>
                <span style={{ fontWeight: 400 }}>
                  When you visit Engleezi’s Web sites, the Company collects your
                  Internet Protocol (“IP”) addresses to track and aggregate
                  non-personal information. For example, Engleeziuses IP
                  addresses to monitor the regions from which Customers and
                  Visitors navigate the Company’s Web sites.
                </span>
              </p>
              <p>
                <span style={{ fontWeight: 400 }}>
                  Engleezi also collects IP addresses from Customers when they
                  log into the Services as part of the Company’s “Identity
                  Confirmation” and “IP Range Restrictions” security features.
                </span>
              </p>
              <p>&nbsp;</p>
              <p>
                <b>Social Media Features</b>
              </p>
              <p>
                <span style={{ fontWeight: 400 }}>
                  The Company’s Web sites may use social media features, such as
                  the Facebook ‘like’ button (“Social Media Features”). These
                  features may collect your IP address and which page you are
                  visiting on the Company’s Web site, and may set a cookie to
                  enable the feature to function properly. You may be given the
                  option by such Social Media Features to post information about
                  your activities on the Company’s Web site to a profile page of
                  yours that is provided by a third party Social Media network
                  in order to share with others within your network. Social
                  Media Features are either hosted by a third party or hosted
                  directly on the Company’s Web site. Your interactions with
                  these features are governed by the privacy policy of the
                  company providing the relevant Social Media Features.
                </span>
              </p>
              <p>&nbsp;</p>
              <p>
                <b>Do Not Track</b>
              </p>
              <p>
                <span style={{ fontWeight: 400 }}>
                  Currently, various browsers — including Internet Explorer,
                  Firefox, and Safari — offer a “do not track” or “DNT” option
                  that relies on a technology known as a DNT header, which sends
                  a signal to Web sites’ visited by the user about the user’s
                  browser DNT preference setting. Engleezi does not currently
                  commit to responding to browsers’ DNT signals with respect to
                  the Company’s Web sites, in part, because no common industry
                  standard for DNT has been adopted by industry groups,
                  technology companies or regulators, including no consistent
                  standard of interpreting user intent. Engleezi takes privacy
                  and meaningful choice seriously and will make efforts to
                  continue to monitor developments around DNT browser technology
                  and the implementation of a standard.
                </span>
              </p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <h4>6. Sharing of information collected</h4>
              <p>&nbsp;</p>
              <p>
                <b>Service Providers</b>
              </p>
              <p>
                <span style={{ fontWeight: 400 }}>
                  Engleezi may share Data about Engleezi Visitors, Customers and
                  Attendees with the Company’s contracted service providers so
                  that these service providers can provide services on our
                  behalf. Without limiting the foregoing, Engleezi may also
                  share Data about Engleezi Visitors, Customers and Attendees
                  with the Company’s service providers to ensure the quality of
                  information provided, and with third–party social networking
                  and media Web sites, such as Facebook, for marketing and
                  advertising on those Web sites. Unless described in this
                  Privacy Statement, Engleezi does not share, sell, rent, or
                  trade any information with third parties for their promotional
                  purposes.
                </span>
              </p>
              <p>&nbsp;</p>
              <p>
                <b>Engleezi.com Affiliates </b>
              </p>
              <p>
                <span style={{ fontWeight: 400 }}>
                  The Company may share Data about Engleezi Customers with other
                  companies in order to work with them, including affiliates of
                  the Engleezi corporate group. For example, the Company may
                  need to share Data about Engleezi Customers for customer
                  relationship management purposes.
                </span>
              </p>
              <p>&nbsp;</p>
              <p>
                <b>Business Partners</b>
              </p>
              <p>
                <span style={{ fontWeight: 400 }}>
                  From time to time, Engleezi may partner with other companies
                  to jointly offer products or services, such as our Marketplace
                  partners. If you purchase or specifically express interest in
                  a jointly–offered product or service from or through Engleezi,
                  the Company may share Data about Engleezi Customers collected
                  in connection with your purchase or expression of interest
                  with our partner(s). Engleezi does not control our business
                  partners’ use of the Data about Engleezi Customers we collect,
                  and their use of the information will be in accordance with
                  their own privacy policies. If you do not wish for your
                  information to be shared in this manner, you may opt not to
                  purchase or specifically express interest in a jointly offered
                  product or service.
                </span>
              </p>
              <p>
                <span style={{ fontWeight: 400 }}>
                  Engleezi does not share Data about Engleezi Attendees with
                  business partners unless: (1) you specifically opt in to such
                  sharing via an event registration form; or (2) you attend a
                  Company event and allow Engleezi or any of its designees to
                  scan your attendee badge. If you do not wish for your
                  information to be shared in this manner, you may choose not to
                  opt in via event registration forms and elect not to have your
                  badge scanned at Company events. &nbsp;If you choose to share
                  your information with business partners in the manners
                  described above, your information will be subject to the
                  business partners’ respective privacy statements.
                </span>
              </p>
              <p>&nbsp;</p>
              <p>
                <b>Third Parties</b>
              </p>
              <p>
                <span style={{ fontWeight: 400 }}>
                  Section 4 of this Privacy Statement, Web Site Navigational
                  Information, specifically addresses the information we or
                  third parties collect through cookies and Web Analytics, and
                  how you can control cookies through your Web browsers.
                </span>
              </p>
              <p>&nbsp;</p>
              <p>
                <b>Billing</b>
              </p>
              <p>
                <span style={{ fontWeight: 400 }}>
                  Engleezi uses a third—party service provider to manage credit
                  card processing. This service provider is not permitted to
                  store, retain, or use Billing Information except for the sole
                  purpose of credit card processing on the Company’s behalf.
                </span>
              </p>
              <p>&nbsp;</p>
              <p>
                <b>Compelled Disclosure</b>
              </p>
              <p>
                <span style={{ fontWeight: 400 }}>
                  Engleezi reserves the right to use or disclose information
                  provided if required by law or if the Company reasonably
                  believes that use or disclosure is necessary to protect the
                  Company’s rights and/or to comply with a judicial proceeding,
                  court order, or legal process.
                </span>
              </p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <h4>7. International transfer of information collected</h4>
              <p>&nbsp;</p>
              <p>
                <span style={{ fontWeight: 400 }}>
                  The Company primarily stores Data about Engleezi Customers and
                  Data about Engleezi Attendees on Amazon Web Services. To
                  facilitate Engleezi’s global operations, the Company may
                  transfer and access such information from around the world,
                  including from other countries and/or hosting companies in
                  which the Company has operations. A list of the Company’s
                  global offices is available here. This Privacy Statement shall
                  apply even if Engleezi transfers Data about Engleezi Customers
                  or Data about Engleezi Attendees to other countries and/or
                  hosting companies.
                </span>
              </p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <h4>8. Communications preferences</h4>
              <p>&nbsp;</p>
              <p>
                <span style={{ fontWeight: 400 }}>
                  Engleezi offers Visitors, Customers, and Attendees who provide
                  contact information a means to choose how the Company uses the
                  information provided. You may manage your receipt of marketing
                  and non-transactional communications by clicking on the
                  “unsubscribe” link located on the bottom of the Company’s
                  marketing emails. Additionally, you may send a request to{' '}
                </span>
                <span style={{ fontWeight: 400 }}>
                  info@myengleezi.com
                </span>
                <span style={{ fontWeight: 400 }}>.</span>
              </p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <h4>9. Correcting and updating your information</h4>
              <p>&nbsp;</p>
              <p>
                <span style={{ fontWeight: 400 }}>
                  Customers may update or change their registration information
                  by editing their user or organization record. To update a user
                  profile, please login to http://www.myengleezi.com with your
                  username and password and click “Settings.” To update an
                  organization’s information, please login to
                  http://www.myengleezi.com with your username and password and
                  select “Organization Setting.” &nbsp;Attendees may update or
                  change their registration information on the event’s Web site
                  after logging in. To update Billing Information or have your
                  registration information deleted, please email
                  info@myengleezi.com. To discontinue your account and to have
                  information you maintained in the Services returned to you,
                  please email{' '}
                </span>
                <span style={{ fontWeight: 400 }}>info@myengleezi.com</span>
                <span style={{ fontWeight: 400 }}>
                  . &nbsp;Requests to access, change, or delete your information
                  will be handled within 30 days.
                </span>
              </p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <h4>10. Customer Data</h4>
              <p>&nbsp;</p>
              <p>
                <span style={{ fontWeight: 400 }}>
                  Engleezi’s Customers may electronically submit data or
                  information to the Services for hosting and processing
                  purposes (“Customer Data”). Engleezi will not review, share,
                  distribute, or reference any such Customer Data except as
                  provided in Engleezi’s Master Subscription Agreement, or as
                  may be required by law. In accordance with Engleezi’s Master
                  Subscription Agreement, Engleezi may access Customer Data only
                  for the purpose of providing the Services or preventing or
                  addressing service or technical problems or as may be required
                  by law.{' '}
                </span>
              </p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <h4>11. Changes to this Privacy Statement</h4>
              <p>&nbsp;</p>
              <p>
                Engleezi reserves the right to change this Privacy Statement.
                Engleezi will provide notification of the material changes to
                this Privacy Statement through the Company’s Web sites at least
                thirty (30) business days prior to the change taking effect.
              </p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <h4>12. Limitation of liability</h4>
              <p>&nbsp;</p>
              <p>
                <span style={{ fontWeight: 400 }}>
                  Under no circumstances, including but not limited to
                  negligence, shall we be liable for any special or
                  consequential damages that results from the use of, or the
                  inability to use, the website and the materials in it.
                </span>
              </p>
              <p>
                <span style={{ fontWeight: 400 }}>
                  Without derogating the aforementioned, in no event shall our
                  total liability exceed the amount paid by you, if any, for
                  accessing the site.
                </span>
              </p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <h4>13. Contacting us</h4>
              <p>&nbsp;</p>
              <p>
                <span style={{ fontWeight: 400 }}>
                  Questions regarding this Privacy Statement or the information practices of the Company’s Web sites should be directed to info@myengleezi.com
                </span>
              </p>

              <p>&nbsp;</p>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
export default Policy
