import React, { Component } from 'react'
import sectionImg from './contactForm.png'
import { Icon } from 'react-icons-kit'
import { arrowRight2 } from 'react-icons-kit/icomoon/arrowRight2'
import { navigate } from "gatsby-link";


function encode(data) {
  return Object.keys(data)
    .map(key => encodeURIComponent(key) + "=" + encodeURIComponent(data[key]))
    .join("&");
}

const mailto = 'mailto:'
class ContactForm extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  }

  handleSubmit = e => {
    e.preventDefault();
    const form = e.target;
    fetch("/", {
      method: "POST",
      headers: { "Content-Type": "application/x-www-form-urlencoded" },
      body: encode({
        "form-name": form.getAttribute("name"),
        ...this.state
      })
    })
      .then(() => navigate(form.getAttribute("action")))
      .catch(error => alert(error));
  }

  render() {
    return (
      <section>
        <div className="container">
          <div className="row">
            <div className=" col-md-4 margin-10px-lr margin-auto  col-sm-6 ">
              <img src={sectionImg} alt="" />
            </div>
            <div className=" col-md-8  col-sm-12 form-group">
              <h1 className="margin-30px-left text-left">{this.props.header}</h1>

              <p className="margin-30px-left text-left">
                {this.props.text}
                <br />{' '}
                <a className="engleezi-black" href={mailto + this.props.link}>
                  {' '}
                  <b> {this.props.link} </b>{' '}
                </a>{' '}
              </p>
              <form name="Engleezi website form" method="POST" data-netlify="true" data-netlify-honeypot="bot-field" action="/contact-us/"
                onSubmit={this.handleSubmit}
              >
                <div className=" col-md-12 col-sm-12 form-group">
                  <label className="col-md-12">
                    <p className="text-black text-left">{this.props.label.name}</p>
                    <input
                      maxLength="40"
                      name="name"
                      type="text"
                      onChange={this.handleChange}
                      placeholder={this.props.label.namePlaceholder}
                      className="box-shadow button-white input-12 pr-0 pl-3"
                    />
                  </label>
                  <label className="col-md-12">
                    <p className="circularstd-book text-black text-left  ">
                      {this.props.label.email}
                    </p>
                    <input
                      maxLength="40"
                      name="email"
                      type="email"
                      onChange={this.handleChange}
                      placeholder={this.props.label.emailPlaceholder}
                      className=" box-shadow  button-white input-12 pr-0 pl-3"
                      required
                    />
                  </label>
                  <label className="col-md-12">
                    <p className="circularstd-book text-black text-left">
                      {this.props.label.phone}
                    </p>
                    <input
                      maxLength="40"
                      name="phone"
                      type="text"
                      onChange={this.handleChange}
                      placeholder={this.props.label.phonePlaceholder}
                      className=" box-shadow  button-white input-12 pr-0 pl-3"
                      required
                    />
                  </label>
                  <label className="col-md-12">
                    <p className="text-left circularstd-book text-black ">
                      {' '}
                      {this.props.label.massage}
                    </p>
                    <textarea
                      className="form-control box-shadow  button-white input-12 pr-0 pl-3 "
                      rows="3"
                      name="message"
                      onChange={this.handleChange}
                    />
                  </label>
                  <label className="col-md-5 col-sm-4 col-lg-3">
                    <button
                      type="submit"
                      className="button-blue input-12 pr-0 pl-3"
                    >
                      <span className="text-button text-left display-inline col-md-3 col-sm-3  kufi">
                        <b> {this.props.label.send} </b>
                        <Icon
                          size={23}
                          icon={arrowRight2}
                          className="fas fa-arrow-right text-left display-inline col-md-4"
                        />
                      </span>
                    </button>
                  </label>
                </div>
              </form>
            </div>
          </div>
        </div>
      </section>
    )
  }
}




export default ContactForm
