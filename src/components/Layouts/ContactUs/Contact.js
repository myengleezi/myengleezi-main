import React, { Component } from 'react'
import Bannar from './Bannar/banner'
import ContactForm from './ContactForm/ContactForm'
import Particles from 'react-particles-js'
import ParticlesBubble from '../ParticlesBubble'

class Contact extends Component {
  render() {
    const ContactDataAr = this.props.data
    return (
      <div>
        <Particles
          style={{ position: 'fixed', zIndex: 0 }}
          params={ParticlesBubble}
        />
        <Bannar
          header={ContactDataAr.Bannar.headerTextH1}
          para={ContactDataAr.Bannar.paragraphTextP}
          link={ContactDataAr.Bannar.link}
        />
        <ContactForm
          header={ContactDataAr.Form.header}
          text={ContactDataAr.Form.text}
          link={ContactDataAr.Form.link}
          label={ContactDataAr.Form.label}
        />
      </div>
    )
  }
}

export default Contact
