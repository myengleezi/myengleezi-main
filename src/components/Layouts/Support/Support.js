import React, { Component } from 'react'
import Bannar from './Bannar/banner'

class Support extends Component {
  render() {
    const SupportDataAr = this.props.data
    return (
      <div>
        <Bannar
          header={SupportDataAr.Bannar.headerTextH1}
          para={SupportDataAr.Bannar.paragraphTextP}
          button={SupportDataAr.Bannar.button}
        />
      </div>
    )
  }
}

export default Support
