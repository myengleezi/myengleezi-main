import React from 'react'
import bannerRight from './Banner.png'
import './bannar.css'

const Banner = () => (
  <section
    style={{
      width: '100%',
      paddingTop: '0',
    }}
  >
    <div className="main-banner-area">
      <div className="right-bg">
        <img
          src={bannerRight}
          className="img-fluid float-right width-100"
          alt=""
        />
      </div>

      <div className="header-text display-fix">
        <div className="container">
          <div className="row">
            <div className="col-lg-5 col-md-12 col-sm-12">
              <h1
                className="co-blue circularstd-bold no-letter-spacing text-left wow fadeInUp"
                data-wow-delay=".1s"
              >
                Hello and welcome to our blog!{' '}
              </h1>
              <p
                className=" poppins text-left no-letter-spacing wow fadeInUp"
                data-wow-delay=".2s"
              >
                Here we will be providing the latest updates, stories and
                information about Engleezi! Check back from time to time to read
                stories, watch videos and learn more about us! Engleezi - It's
                so Easy!
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
)

export default Banner
