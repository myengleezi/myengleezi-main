import React from 'react'
import BlogPic1 from './blogPic1.png'
import './blog.css'

const blogPost = []
for (let i = 0; i < 4; i++) {
  blogPost.push(
    <div className="col-md-6 col-sm-12 padding-ten-top  ">
      <div>
        <a href="#">
          <img src={BlogPic1} className="img-responsive" alt="Blog Image" />
        </a>
      </div>
      <div>
        <h3>
          <a href="#">We Help You Create Perfect Modern Design</a>
        </h3>
      </div>
      <div className="blog-post-format">
        <span> Author</span>
        <span>
          <i className="fa fa-date" /> Feb 7, 2019
        </span>
      </div>
      <div className="blog-post-des">
        <p className="blog-p">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut
          aliquip ex ea commodo consequat. Duis aute irure dolor in
          reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
          pariatur.
        </p>
        <button className="button-light">
          <span className="text-button  text-black col-md-8 kufi">
            <a href="#">Continue Reading</a>{' '}
          </span>
          <i className="fas fa-arrow-right text-right display-inline col-md-4" />
        </button>
      </div>
    </div>
  )
}
const BlogPage = () => (
  <section>
    <div className="container">
      <div className="row">
        <div className="col-md-12 col-sm-12 text-center ">
          <p>
            Each classes are 25 minutes long, and learning is conducted through
            songs, games and real time interactions. Furthermore, completed
            sessions can be reviewed once a student has completed a new class to
            further improve their English language learning skills. This makes
            our classes very engaging and fun at the same time!
          </p>
        </div>
      </div>
    </div>

    <div className="container">
      <div className="row">
        {blogPost.map(post => {
          return post
        })}
      </div>
    </div>
  </section>
)

export default BlogPage
