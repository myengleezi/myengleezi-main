import React, { Component } from 'react'
import section2Img from './section2.png'
export class Join extends Component {
  render() {
    return (
      <section>
        <div className="container">
          <div className="d-flex flex-md-row-reverse row">
            <div className=" col-md-5 col-sm-12">
              <img
                src={section2Img}
                alt="Get Instant results with Engleezi"
                className="col-sm-8 col-md-12 "
              />
            </div>
            <div className="offset-md-1 col-md-6 col-sm-12 margin-ten-top">
              <h2 className="text-left margin-five-bottom engleezi-black">
                {this.props.header}
              </h2>

              <p className="text-left font-weight-400 engleezi-black">
                {this.props.text}
              </p>
            </div>
          </div>
        </div>
      </section>
    )
  }
}

export default Join
