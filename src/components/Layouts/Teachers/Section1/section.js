import React from 'react'
import section1Img from './section1.png'

const Services = props => (
  <section>
    <div className="container">
      <div className="row">
        <div className=" col-md-5 col-sm-12">
          <img
            src={section1Img}
            alt="Get Instant results with Engleezi"
            className="col-sm-8 col-md-12 "
          />
        </div>
        <div className="offset-md-1 col-md-6 col-sm-12 margin-ten-top">
          <h2 className="text-left margin-five-bottom engleezi-black">
            {props.header}
          </h2>

          <p className="font-weight-400 engleezi-black">{props.text}</p>
        </div>
      </div>
    </div>
  </section>
)

export default Services
