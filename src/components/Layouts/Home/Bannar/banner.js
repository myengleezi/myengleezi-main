import React from 'react'
import bannerRight from './Banner.png'
import { Icon } from 'react-icons-kit'
import { arrowRight2 } from 'react-icons-kit/icomoon/arrowRight2'
import './section.css'

const Banner = props => (
  <section style={{paddingTop:0}}>
    <div className="main-banner-area bannar-background">
      <div className="header-main-text">
        <div className="container">
          <div className="row " >
          
            <div className="col-lg-5 col-md-10 col-sm-12">
              <h1
                className="text-left main-h circularstd-bold co-blue wow fadeInUp"
                data-wow-delay=".1s"
              >
                {props.heading}
              </h1>
              <p
                className="text-left main-p poppins wow fadeInUp"
                data-wow-delay=".2s"
              >
                {props.para}
              </p>

              <div className="divider-full" />
              <div className="text-left fadeInUp" data-wow-delay=".3s">
                <div className="input">
                  <a
                    href="https://student.myengleezi.com/HumanResource/login.aspx"
                    target="_blank"
                  >
                    <button className="button-light">
                      <span
                        className=" kufi text-button text-black col-md-8"
                        style={{ color: 'black' }}
                      >
                        {props.button}
                      </span>
                      <span className="co-blue">
                        <Icon
                          size={25}
                          icon={arrowRight2}
                          className="fas fa-arrow-right text-right display-inline col-md-4"
                        />
                      </span>
                    </button>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
    )


export default Banner
