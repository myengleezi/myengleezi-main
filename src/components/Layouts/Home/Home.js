import React, { Component } from 'react'
import Particles from 'react-particles-js'
import ParticlesBubble from '../ParticlesBubble'
import Services from './Services/services'
import Bannar from './Bannar/banner'
import Whyus from './WhyUs/whyUs'
import Blog from './Blog/Blog'
import Join from './Join/Join'
class Home extends Component {
  render() {
    const HomeDataAr = this.props.data
    return (
      <>
        <Particles
          style={{ position: 'fixed', zIndex: 0 }}
          params={ParticlesBubble}
        />
        <Bannar
          heading={HomeDataAr.Bannar.headerTextH1}
          para={HomeDataAr.Bannar.paragraphTextP}
          button={HomeDataAr.Bannar.buttonText}
        />
        <Services
          serviceA={HomeDataAr.Services.service1}
          serviceB={HomeDataAr.Services.service2}
          serviceC={HomeDataAr.Services.service3}
        />
        <Whyus
          reason={HomeDataAr.WhyUs.headingText}
          list={HomeDataAr.WhyUs.listItems}
        />
        <Blog />
        <Join
          heading={HomeDataAr.Join.headingText}
          buttonText={HomeDataAr.Join.buttonText}
        />
      </>
    )
  }
}
export default Home
