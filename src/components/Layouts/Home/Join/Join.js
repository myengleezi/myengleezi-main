import React, { Component } from 'react'
import joinNow from './joinNow.png'
import { Icon } from 'react-icons-kit'
import { arrowRight2 } from 'react-icons-kit/icomoon'

export class Join extends Component {
  render() {
    return (
      <div>
        <div className="container">
          <div className="d-flex flex-md-row-reverse row">
            <div className=" img-responsive col-md-5 col-sm-10">
              <img src={joinNow} alt="Join To Engleezi Now" />
            </div>
            <div className="col-sm-8  col-md-6 margin-twenty-top ">
              <h1>{this.props.heading}</h1>
              <a
                href="https://student.myengleezi.com/HumanResource/login.aspx"
                target="_blank"
              >
                <button className="button-blue col-lg-5 col-md-7 col-sm-8 ">
                  <span>{this.props.buttonText}</span>
                  <Icon
                    size={25}
                    icon={arrowRight2}
                    className="fas fa-arrow-right text-left display-inline col-md-4"
                  />
                </button>
              </a>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Join
