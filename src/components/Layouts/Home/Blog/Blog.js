import React from 'react'
import blogPc1 from './blogPc1.jpg'
import blogPc2 from './blogPc2.jpg'
import blogPc3 from './blogPc3.jpg'
import './blog.css'
import { Icon } from 'react-icons-kit'
import { arrowRight2 } from 'react-icons-kit/icomoon/arrowRight2'

function Blog(props) {
  return (
    <section>
      <div className="container">
       
          <h6 className="text-blue text-center "> Our Blog</h6>
      
        
          <h3 className="section-heading text-center">Latest Blog Posts</h3>
     
        <div className="row">
          <div className="col-lg-4 col-md-12  col-sm-12  sm-margin-30px-bottom">
            <div className=" card-shadow card-view">
              <div className="padding-five-bottom">
                <img className="card-img-top" src={blogPc1} alt="blog " />
              </div>
              <p className="margin-two-right text-blue margin-six-left text-left">
                <span> Engleezi | 18 April 2019 </span>
              </p>
              <div className="card-body">
                <h6 className="text-blue text-left ">
                  {' '}
                  Learning should be fun!                
                  </h6>
                <p className="card-text text-left " style={textCard}>
                Toshio Co-Founder, Engleezi I grew up in Canada and it was mandatory for me to learn French in school as it is our countries second official language. I started learning when I was 12 years old and finished when I was 17 and I disliked every second of it!
                </p>
                <h6 className="text-left">
                  <a
                    href="https://blog.myengleezi.com/learning-should-be-fun/"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <span className="text-blue "> Read More </span>
                  </a>
                  <i className="fas fa-arrow-right text-right col-md-4 display-inline  text-blue" />
                </h6>
              </div>
            </div>
          </div>

          <div className="col-lg-4 col-md-12  col-sm-12 sm-margin-30px-bottom">
            <div className=" card-shadow card-view">
              <div className="padding-five-bottom">
                <img className="card-img-top" src={blogPc2} alt="blog " />
              </div>
              <p className="margin-two-right margin-six-left text-blue text-left ">
                <span>  Engleezi | 18 April 2019 </span>
              </p>
              <div className="card-body">
                <h6 className="text-blue text-left ">
                  {' '}
                  Parents should do homework too!{' '}
                </h6>
                <p className="card-text text-left " style={textCard}>
                Toshio Co-Founder, Engleezi One thing that is a universal truth is that kids hate homework. Wherever you go, no child wants to do homework. Most find excuses to get out of it. Others do it, but hesitantly. 
                </p>
                <h6 className="text-left">
                  <a
                    href="https://blog.myengleezi.com/parents-should-do-homework-too/"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <span className="text-blue text-left"> Read More </span>
                  </a>
                  <i className="fas fa-arrow-right text-right col-md-4 display-inline text-blue" />
                </h6>
              </div>
            </div>
          </div>

          <div className="col-lg-4 col-md-12 col-sm-12 sm-margin-30px-bottom">
            <div className=" card-view">
              <div className="padding-five-bottom">
                <img className="card-img-top" src={blogPc3} alt="blog " />
              </div>
              <p className="margin-two-right margin-six-left text-blue text-left">
              <span>  Engleezi | 18 April 2019 </span>
              </p>
              <div className="card-body">
                <h6 className="text-blue text-left">
                  {' '}
                  Is learning from home the same as in a class? </h6>
                <p className="card-text text-left" style={textCard}>
                Toshio Co-Founder, Engleezi The traditional method of learning a language is in a classroom. You go to class with your homework done, pen and paper and listen to the teacher explain the days lesson....
                </p>
                <h6 className="text-left">
                  <a
                    href="https://blog.myengleezi.com/is-learning-from-home-the-same-as-in-a-classroom/"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <span className="text-blue text-left">
                      {' '}
                      Read More {' '}
                    </span>
                  </a>
                  <i className="fas fa-arrow-right text-right col-md-4 display-inline  text-blue" />
                </h6>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="margin-lr-auto margin-tb margin-three-top">
          <a
            href="https://blog.myengleezi.com/"
            rel="noopener noreferrer"
            target="_blank"
          >
            <button className="button-light">
              <span className="text-button  text-black col-md-8 ">
View All 
                <Icon
                  size={23}
                  icon={arrowRight2}
                  className="fas fa-arrow-right text-right display-inline col-md-4 co-blue"
                />
              </span>
            </button>
          </a>
        </div>
      </div>
    </section>
  )
}

Blog.propTypes = {}
const textCard = {
  height: '50px',
  overflow: 'hidden',
}

export default Blog
