import React from 'react'
import PropTypes from 'prop-types'
import { StaticQuery, graphql } from 'gatsby'
import Header from './Header/header'

import Footer from './Footer/footer'
import Chat from './chat'
import './layout.css'

const Layout = ({ children }) => (
  <StaticQuery
    query={graphql`
      query SiteTitleQuery {
        site {
          siteMetadata {
            title
          }
        }
      }
    `}
    render={data => (
      <>
        <Header
          links={arlinks}
          siteTitle={data.site.siteMetadata.title}
          style={{ zIndex: 5 }}
        />

        <main>{children}</main>
        <Chat />
        <Footer />
      </>
    )}
  />
)

const arlinks = [
  { label: 'Home', link: '/' },
  { label: 'Teachers', link: '/teachers/' },
  { label: 'Curriculum', link: '/curriculum/' },
  { label: 'How to Start', link: '/how-to-start/' },
  { label: 'Support', link: '/support/' },
  { label: 'Contact Us', link: '/contact-us/' },
]

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
