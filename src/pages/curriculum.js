import React, { Component } from 'react'
import './default.css'
import Layout from '../components/layout'
import Curriculum from '../components/Layouts/Curriculum/Curriculum'
import 'bootstrap/dist/css/bootstrap.min.css'
import SEO from '../components/seo'
import CurriculumData from '../Data/curriculumData'

class CurriculumPage extends Component {
  render() {
    const data = CurriculumData.En
    return (
      <Layout>
        <SEO
          title="Our Curriculum"
          keywords={[
            'English',
            'english learning',
            'english learning from home',
            'english for my child',
            'english for my children',
            'english for my kid',
            'english for my kids',
            'ielts',
            'IB',
            'learning english',
            'easy english',
            'learn english',
            'native english',
            'fluent in english',
            'english for parents',
            'how to teach english',
            'online english',
            'affordable english',
            'fun english learning',
            'fun english',
            'english tutoring',
            'english tutors',
            'english tutoring service',
            'better english',
            'english speaker',
            'native English teacher',
            'english teacher',
            'Canada english teacher',
            'US english teacher',
            'english family',
          ]}
        />
        <Curriculum data={data} />
        <div style={{ maxWidth: `300px`, marginBottom: `1.45rem` }} />
      </Layout>
    )
  }
}
export default CurriculumPage
