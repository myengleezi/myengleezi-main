const aboutData = {
  En: {
    Bannar: {
      headerTextH1: 'About Us',
      paragraphTextP:
        'Engleezi is an innovative, state of the art online learning platform that facilitates high quality, one-on-one English lessons for children. Not only are our classes fun and intuitive, they are also designed to be both affordable and convenient for everyone. With Engleezi, your child can now learn English from the comfort of your own home.',
    },
  },
}
export default aboutData
