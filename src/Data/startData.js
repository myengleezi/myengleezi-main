const startData = {
  En: {
    Bannar: {
      headerTextH1: 'How to Start',
      paragraphTextP: " It's as easy as One, Two, Three!",
      buttonText: 'Sign Up Now',
    },
    Steps: {
      step1: 'Login to our platform and choose your desired class',
      step2: ' Select your preferred time and date',
      step3: 'You’re ready to learn!',
    },
    Bonus: {
      bonusHeading: 'Bonus',
      bonusText:
        'Sign up now and receive a free class! Or even better, earn by referring a friend to Engleezi! ',
      bonusImportant: 'Disclaimer: Up to 10 referrals a year only',
      bonusButton: 'I am interested',
    },
  },
}
export default startData
