const curriculumData = {
  En: {
    Bannar: {
      headerTextH1: 'Our Curriculum',
      paragraphTextP:
        ' With over 100 classes to follow, our revolutionary platform is designed for both teachers and students to converse and learn in real-time. This guarantees a unique experience like no other!',
      buttonText: 'Find Out More',
    },
    Result: {
      header: 'Get Instant Results',
      text:
        'Upon completing the course, a participant would have mastered 50-percent of the English speech, be familiarised with every sound and achieved an A2 level of English according to the The Common European Framework of Reference for Languages (CEFR)',
    },
    Structure: {
      header: 'Class Structure',
      text:
        'Each class is 25 minutes long, and learning is conducted through songs, games and real time interactions. This makes our classes very engaging and fun at the same time!',
    },
  },
}
export default curriculumData
