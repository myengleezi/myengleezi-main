const supportData = {
  En: {
    Bannar: {
      headerTextH1: 'Support',
      paragraphTextP:
        ' Our admin and teachers are always ready to provide assistance and monitor your child’s progress in mastering the English language. After completing a class, we provide additional learning materials as homework and an assessment of your child’s progress.',
      button: 'Contact Now ',
    },
  },
}
export default supportData
