const curriculumData = {
  En: {
    Bannar: {
      headerTextH1: 'Need Help?',
      paragraphTextP: ' Feel free to contact us at',
      link: 'info@myengleezi.com',
    },
    Form: {
      header: 'Contact Us',
      text: 'Reach out to one of our staff at',
      link: 'info@myengleezi.com',
      label: {
        name: 'FULL NAME',
        namePlaceholder: 'How Should We Address You',
        email: 'EMAIL ADDRESS',
        emailPlaceholder: 'Enter A vaild Email Address',
        phone: 'PHONE NUMBER',
        phonePlaceholder: 'Your Current Phone Number',
        massage: 'MESSAGE',
        send: 'Submit',
      },
    },
  },
}
export default curriculumData
