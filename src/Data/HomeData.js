const HomeData = {
  En: {
    Bannar: {
      headerTextH1: 'Engleezi - Your Ultimate Guide To Learn English',
      paragraphTextP:
        'English is an important language spoken and understood by many across the globe. Though learning it could be difficult for some, we at Engleezi believe that learning English should be both fun and exciting!',
      buttonText: 'I am interested',
    },
    Services: {
      service1: ' 25 Minutes  for each class.',
      service2: ' 100+ classes to follow.',
      service3: 'Achieve A2 Level (CEFR) of the English Language.',
    },
    WhyUs: {
      headingText: 'Why Choose Us',
      listItems: [
        'Our qualified English teachers are trained to communicate and teach students from around the world.',
        ' All syllabuses are designed for both new or intermediate English language learners.',
        ' Learn English from the comfort of your own home, anytime.',
        ' We make learning English both affordable and accessible.',
      ],
    },
    Join: {
      headingText: 'Ready To Join Us',
      buttonText: 'Sign Up Now',
    },
  },
}

export default HomeData
