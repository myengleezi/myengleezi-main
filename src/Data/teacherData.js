const teacherData = {
  En: {
    Bannar: {
      headerTextH1: 'Our Teachers',
      paragraphTextP:
        ' We’re not just teachers, but in fact, our team is made up of professionals with different backgrounds ranging from education, business, IT, marketing and more. Most importantly, we all share the same passion, and that passion is the love of language learning!',
      buttonText: 'Find Out More',
    },
    Section1: {
      header: 'Qualified English speakers with intensive training.',
      text:
        'Our teachers will guide and coach your child to discover their English learning potential.',
    },
    Section2: {
      header: 'Focus on Pronunciation.',
      text:
        'Besides learning vocabulary, pronunciation is important. Our syllabus will improve your child’s command of the English language.',
    },
    Section3: {
      header: 'Intuitive and Convenient.',
      text:
        'No more driving out to tuition classes! Just get on your mobile, tablet or computer and start learning immediately with one of our teachers from home!',
    },
  },
}
export default teacherData
